"""Hello unit test module."""

from extractor.hello import hello


def test_hello():
    """Test the hello function."""
    assert hello() == "Hello extractor"
